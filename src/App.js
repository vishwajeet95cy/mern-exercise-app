import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";
import ExerciseList from './components/Exercises-List';
import EditExercise from './components/Edit-Exercise';
import CreateExercise from './components/Create-Exercise';
import CreateUser from './components/Create-User';
import Navbar from './components/Navbar';



function App() {
  return (
    <Router>
      <div className="container">
        <Navbar />
        <Route path="/" exact component={ExerciseList} />
        <Route path="/edit/:id" component={EditExercise} />
        <Route path="/create" exact component={CreateExercise} />
        <Route path="/user" exact component={CreateUser} />
      </div>
    </Router>
  );
}

export default App;
