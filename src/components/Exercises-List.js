import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Exercise = props => {
  return <tr>
    <td>{props.data.username}</td>
    <td>{props.data.description}</td>
    <td>{props.data.duration}</td>
    <td>{props.data.date.substring(0, 10)}</td>
    <td>
      <Link to={"/edit/" + props.data._id}>edit</Link> | <a href="#" onClick={() => { props.deleteExercise(props.data._id) }}>delete</a>
    </td>
  </tr>
}

export class ExerciseList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      exercises: []
    }

    this.deleteExercise = this.deleteExercise.bind(this)
  }

  componentDidMount() {
    axios.get('http://localhost:8000/exercises/').then(res => {
      this.setState({
        exercises: res.data
      })
    }).catch((err) => {
      console.log(err)
    })
  }

  deleteExercise(id) {
    axios.delete('http://localhost:8000/exercises/' + id)
      .then(res => console.log(res.data));
    this.setState({
      exercises: this.state.exercises.filter(el => el._id !== id)
    })
  }

  exerciseList() {
    return this.state.exercises.map(current => {
      return <Exercise data={current} deleteExercise={this.deleteExercise} key={current._id} />;
    })
  }

  render() {
    return (
      <div>
        <h3>Logged Exercises</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Username</th>
              <th>Description</th>
              <th>Duration</th>
              <th>Date</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.exerciseList()}
          </tbody>
        </table>
      </div>
    )
  }
}

export default ExerciseList
